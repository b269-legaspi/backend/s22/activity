/*
    Create functions which can manipulate our arrays.
*/

let registeredUsers = [
  "James Jeffries",
  "Gunther Smith",
  "Macie West",
  "Michelle Queen",
  "Shane Miguelito",
  "Fernando Dela Cruz",
  "Akiko Yukihime",
];

let friendsList = [];

/*
	
 1. Create a function which will allow us to register into the registeredUsers list.
			- this function should be able to receive a string.
			- determine if the input username already exists in our registeredUsers array.
					-if it is, show an alert window with the following message:
							"Registration failed. Username already exists!"
					-if it is not, add the new username into the registeredUsers array and show an alert:
							"Thank you for registering!"
			- invoke and register a new user.
			- outside the function log the registeredUsers array.

*/

const registerUser = function (user) {
  if (registeredUsers.includes(user) === true) {
    alert("Registration failed. Username already exists!");
  } else {
    registeredUsers.push(user);
    alert("Thank you for registering!");
  }
};

console.log(registeredUsers);

/*
	2. Create a function which will allow us to add a registered user into our friends list.
			- this function should be able to receive a string.
			- determine if the input username exists in our registeredUsers array.
					- if it is, add the foundUser in our friendList array.
									-Then show an alert with the following message:
											- "You have added <registeredUser> as a friend!"
					- if it is not, show an alert window with the following message:
							- "User not found."
			- invoke the function and add a registered user in your friendsList.
			- Outside the function log the friendsList array in the console.

*/

const addFriend = function (user) {
  const foundUser = registeredUsers.includes(user);
  if (foundUser === true) {
    friendsList.push(user);
    alert("You have added " + user + " as a friend!");
  } else {
    alert("User not found");
  }
};

/*
	3. Create a function which will allow us to show/display the items in the friendList one by one on our console.
			- If the friendsList is empty show an alert: 
					- "You currently have 0 friends. Add one first."
			- Invoke the function.

*/

const showFriends = function () {
  if (friendsList.length === 0) {
    alert("You currently have 0 friends. Add one first.");
  } else {
    friendsList.forEach(function (el) {
      console.log(el);
    });
  }
};

/*
	4. Create a function which will display the amount of registered users in your friendsList.
			- If the friendsList is empty show an alert:
					- "You currently have 0 friends. Add one first."
			- If the friendsList is not empty show an alert:
					- "You currently have <numberOfFriends> friends."
			- Invoke the function

*/

const showFriendsAmount = function () {
  if (friendsList.length === 0) {
    alert("You currently have 0 friends. Add one first.");
  } else {
    alert("You currently have " + friendsList.length + " friends.");
  }
};

/*
	5. Create a function which will delete the last registeredUser you have added in the friendsList.
			- If the friendsList is empty show an alert:
					- "You currently have 0 friends. Add one first."
			- Invoke the function.
			- Outside the function log the friendsList array.

*/

const deleteLastFriend = function () {
  if (friendsList.length === 0) {
    alert("You currently have 0 friends. Add one first.");
  } else {
    friendsList.pop();
  }
};

/*
	Stretch Goal:

	Instead of only deleting the last registered user in the friendsList delete a specific user instead.
			-You may get the user's index.
			-Then delete the specific user with splice().

*/

const deleteSpecificUser = function (index) {
  if (friendsList.length === 0) {
    alert("You currently have 0 friends. Add one first.");
  } else {
    friendsList.splice(index, 1);
  }
};

// testing
// addFriend("James Jeffries");
// addFriend("Michelle Queen");
// addFriend("Fernando Dela Cruz");
// addFriend("Gunther Smith");

// deleteSpecificUser(2);

/////////////////////////

// // console.log("Hello World!");

// // [SECTION] Arrays Methods
// // Javascript has built-in functions and methods for arrays.
// // This allows us to manipulate and access array items.

// // [SECTION] Mutator Methods
// // Mutator methods are functions that "mutate" or change an array after they're created

// let fruits = ["Apple", "Banana", "Cherry", "Dates"];

// console.log("Current array:");
// console.log(fruits);
// // (4)['Apple', 'Banana', 'Cherry', 'Dates']

// // push()
// // Adds an element in the end of an array AND returns the array's length
// /*
// SYNTAX:
// 	arrayName.push();
// */
// let fruitsLength = fruits.push("Guava");
// console.log(fruitsLength); // 5
// console.log("Mutated array from push method:");
// console.log(fruits);
// // (5)['Apple', 'Banana', 'Cherry', 'Dates', 'Guava']
// // Guava should be added at the end of the array

// // Adding multiple elements to an array
// fruits.push("Jackfruit", "Kiwi");
// console.log("Mutated array from push method:");
// console.log(fruits);
// // (7)['Apple', 'Banana', 'Cherry', 'Dates', 'Guava', 'Jackfruit', 'Kiwi']
// // Jackfruit and Kiwi should be added at the end of the array

// // pop()
// // - Removes the last element in an array AND returns the removed element
// /*
// SYNTAX:
// 	arrayName.pop();
// */
// let removedFruit = fruits.pop();
// console.log(removedFruit); // Kiwi
// console.log("Mutated array from pop method:");
// console.log(fruits);
// // (6)['Apple', 'Banana', 'Cherry', 'Dates', 'Guava', 'Jackfruit']
// // Kiwi should be removed from the array

// // unshift()
// // - Adds one or more elements at the beginning of an array
// /*
// SYNTAX:
// 	arrayName.unshift('elementA');
// 	arrayName.unshift('elementA', elementB);
// */
// fruits.unshift("Kiwi", "Lime");
// console.log("Mutated array from unshift method:");
// console.log(fruits);
// // Kiwi and Lime should be added in the beginning of an array

// // shift()
// // Removes an element at the beginning of an array AND returns the removed element
// /*
// SYNTAX:
// 	arrayName.shift();
// */
// let anotherFruit = fruits.shift();
// console.log(anotherFruit); // Kiwi
// console.log("Mutated array from shift method:");
// console.log(fruits);
// // (7)['Lime', 'Apple', 'Banana', 'Cherry', 'Dates', 'Guava', 'Jackfruit']
// // Kiwi should be removed from the array

// // splice()
// /*
// SYNTAX
// 	arrayName.splice(startingIndex, deleteCount, elementsToBeAdded)
// */
// // Simultaneously removes elements from a specified index number and adds elements
// fruits.splice(1, 3, "Mango", "Melon");
// console.log("Mutated array from splice method:");
// console.log(fruits);
// // (7)['Lime', 'Mango', 'Melon', 'Cherry', 'Dates', 'Guava', 'Jackfruit']
// // Fruits in index 1 and 2 should be deleted and replaced with Mango and Melon

// // sort()
// // Rearranges the array elements in alphabetical order
// /*
// Syntax
//     arrayName.sort();
// */
// fruits.sort();
// console.log("Mutated array from sort method:");
// console.log(fruits);
// // Fruits should be in alphabetical orders
// // (7)['Cherry', 'Dates', 'Guava', 'Jackfruit', 'Lime', 'Mango', 'Melon']

// // reverse()
// // Reverses the order of array elements
// fruits.reverse();
// console.log("Mutated array from reverse method:");
// console.log(fruits);
// // Fruits should be in alphabetical but in reversed orders
// // (7)['Melon', 'Mango', 'Lime', 'Jackfruit', 'Guava', 'Dates', 'Cherry']

// // [SECTION] Non-Mutator Methods
// // Non-Mutator methods are functions that do not modify or change an array after they're created

// let countries = ["US", "PH", "CAN", "SG", "TH", "PH", "FR", "DE"];
// console.log("Current array:");
// console.log(countries);

// // indexOf()
// // Returns the index number of the first matching element found in an array
// /*
// SYNTAX:
// 	arrayName.indexOf(searchValue);
// 	arrayName.indexOf(searchValue, fromIndex);
// */
// let firstIndex = countries.indexOf("PH");
// console.log("Result of indexOf method: " + firstIndex);

// let invalidCountry = countries.indexOf("DE");
// console.log("Result of indexOf method: " + invalidCountry);

// // lastIndexOf()
// // Returns the index number of the last matching element found in an array
// /*
// SYNTAX
// 	arrayName.lastIndexOf(searchValue);
// 	arrayName.lastIndexOf(searchValue, fromIndex);
// */
// // Getting the index number starting from the last element
// let lastIndex = countries.lastIndexOf("PH");
// console.log("Result of lastIndexOf method: " + lastIndex);

// // Getting the index number starting from a specified index
// let lastIndexStart = countries.lastIndexOf("PH", 6);
// console.log("Result of lastIndexOf method: " + lastIndexStart);

// // slice()
// // Portions/slices elements from an array AND returns a new array
// /*
// SYNTAX:
// 	arrayName.slice(startingIndex);
//     arrayName.slice(startingIndex, endingIndex);
// */
// // Slicing off elements from a specified index to the last element
// let slicedArrayA = countries.slice(2);
// console.log("Result from slice method:");
// console.log(slicedArrayA);
// // (6)['CAN', 'SG', 'TH', 'PH', 'FR', 'DE']
// // US and PH should be sliced

// console.log(countries);
// // ['US', 'PH', 'CAN', 'SG', 'TH', 'PH', 'FR', 'DE']

// // Slicing off elements from a specified index to another index
// // index 2 will be included and index 4 will be excluded
// let slicedArrayB = countries.slice(2, 4);
// console.log("Result from slice method:");
// console.log(slicedArrayB);
// // ['CAN', 'SG']

// // Slicing off elements starting from the last element of an array
// let slicedArrayC = countries.slice(-3);
// console.log("Result from slice method:");
// console.log(slicedArrayC);
// // (3)['PH', 'FR', 'DE']
// // CAN, SG and TH should be sliced from the new array

// // toString()
// // Returns an array as a string separated by commas
// /*
// SYNTAX:
// 	arrayName.toString();
// */
// let stringArray = countries.toString();
// console.log("Result from toString method:");
// console.log(stringArray); // US,PH,CAN,SG,TH,PH,FR,DE

// // concat()
// // Combines two arrays and returns the combined result
// /*
// SYNTAX
// 	arrayA.concat(arrayB);
// 	arrayA.concat(elementA);
// */
// let tasksArrayA = ["drink html", "eat javascript"];
// let tasksArrayB = ["inhale css", "breathe sass"];
// let tasksArrayC = ["get git", "be node"];

// let tasks = tasksArrayA.concat(tasksArrayB);
// console.log("Result from concat method:");
// console.log(tasks);
// // ['drink html', 'eat javascript', 'inhale css', 'breathe sass']

// // Combining multiple arrays
// console.log("Result from concat method:");
// let allTasks = tasksArrayA.concat(tasksArrayB, tasksArrayC);
// console.log(allTasks);
// // ['drink html', 'eat javascript', 'inhale css', 'breathe sass', 'get git', 'be node']

// // Combining arrays with elements
// let combinedTasks = tasksArrayA.concat("smell express", "throw react");
// console.log("Result from concat method:");
// console.log(combinedTasks);
// // ['drink html', 'eat javascript', 'smell express', 'throw react']

// // join()
// // Returns an array as a string separated by specified separator string
// /*
// SYNTAX
// arrayName.join('separatorString');
// */
// let users = ["John", "Jane", "Joe", "Robert"];
// console.log(users);
// console.log(users.join());
// console.log(users.join(", "));
// console.log(users.join(" - "));

// // [SECTION] Iterations Methods
// // Iteration methods are loops designed to perform repetitive tasks on arrays

// // forEach()
// // Similar to a for loop that iterates on each array element
// // Looping through all Array Items
// /*
// SYNTAX
// arrayName.forEach(function(indivElement) {
//     statement
// });
// */
// allTasks.forEach(function (task) {
//   //console.log(task)

//   // If the element/string's length is greater than 10 characters
//   if (task.length > 10) {
//     console.log(task); // eat javascript and breathe sass
//   }
// });

// // map()
// // Iterates on each element AND returns new array with different values depending on the result of the function's operation
// /*
// SYNTAX:
// let/const resultArray = arrayName.map(function(indivElement)){
// 	return expression/condition;
// }
// */

// let numbers = [1, 2, 3, 4, 5];

// let numberMap = numbers.map(function (number) {
//   return number * number;
// });

// console.log("Original Array:");
// console.log(numbers); //Original is unaffected by map()
// // (5)[1, 2, 3, 4, 5]
// console.log("Result of map method:");
// console.log(numberMap); //A new array is returned by map() and stored in the variable.
// // (5)[1, 4, 9, 16, 25]

// // filter()
// // Returns new array that contains elements which meets the given condition
// /*
// SYNTAX
// let/const resultArray = arrayName.filter(function(indivElement) {
//     return expression/condition;
// })
// */
// let filterValid = numbers.filter(function (number) {
//   return number < 4;
// });
// console.log("Result of filter method:");
// console.log(filterValid);

// //includes()
// // includes() method checks if the argument passed can be found in the array.
// // returns true if the argument is found in the array and returns false if it is not
// /*
// SYNTAX
// arrayName.includes(<argumentToFind>)
// */
// let products = ["Mouse", "Keyboard", "Laptop", "Monitor"];

// let productFound1 = products.includes("Mouse");
// console.log(productFound1);

// let productFound2 = products.includes("Headset");
// console.log(productFound2);
